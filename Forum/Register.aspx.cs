﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum.Account
{
    public partial class Register : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            
        }

        protected void CreateUserButton_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = from d in db.Users where d.username.Equals(UserName.Text) select d;

            if (query.Count()>0)
            {
                ErrorMessage.Text = "Username already exists";
            }
            else
            {
                User user = new User();
                user.username = UserName.Text;
                user.password = Password.Text;
                user.email = Email.Text;
                user.firstname = FirstName.Text;
                user.lastname = LastName.Text;
                user.votes = 0;
                user.date = DateTime.Now;
                try
                {
                    db.Users.InsertOnSubmit(user);
                    db.SubmitChanges();
                    Session["LoggedUser"] = user;
                    if (Session["Page"] == null)
                    {
                        Response.Redirect("/Questions.aspx");
                    }
                    else
                    {
                        Response.Redirect("/" + Session["Page"].ToString());
                    }
                }
                catch (Exception)
                {
                    ErrorMessage.Text = "Database error";
                } 
            }
        }

    }
}
