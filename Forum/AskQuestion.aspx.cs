﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class AskQuestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedUser"] == null)
            {
                Session["Page"] = "AskQuestion.aspx";
                Response.Redirect("/Login.aspx");
            }
            else
            {
                User user = (User)Session["LoggedUser"];
                DataClassesDataContext db = new DataClassesDataContext();
                if (DropDownList1.SelectedIndex >= 0 && TextBox1.Text.Length > 4 && TextBox1.Text.Length > 4)
                {
                    Question question = new Question();
                    question.subject = TextBox1.Text;
                    question.text = TextBox2.Text;
                    question.date = DateTime.Now;
                    question.votes = 0;
                    var query = from c in db.Categories where c.categoryname.Equals(DropDownList1.SelectedValue) select c;
                    Category cat = null;
                    foreach (Category category in query)
                    {
                        cat = category;
                        break;
                    }
                    var query2 = from u in db.Users where u.username.Equals(user.username) select u;
                    User u2 = null;
                    foreach (User user2 in query2)
                    {
                        u2 = user2;
                        break;
                    }
                    if (u2 != null && cat != null)
                    {
                        question.User = u2;
                        question.Category = cat;
                        db.Questions.InsertOnSubmit(question);
                        db.SubmitChanges();
                        Response.Redirect("/Answers.aspx?questionid=" + question.questionid);
                    }
                }

            }
        }
    }
}