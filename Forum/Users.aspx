﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Users.aspx.cs" Inherits="Forum.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="User Name:" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label ID="UserName" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Email:" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Email" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Label ID="Label3" runat="server" Text="First Name:" Visible="False"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="FirstName" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Last Name:" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LastName" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Registered:" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Registered" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
