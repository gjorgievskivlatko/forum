﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PlaceHolder1.Controls.Clear();
            DataClassesDataContext db = new DataClassesDataContext();
            if (Request["userid"] == null)
            {
                var query = from u in db.Users select u;
                foreach (User user in query)
                {
                    Literal literal = new Literal();
                    literal.Text = "<p><a href=\"Users.aspx?userid=" + user.userid + "\">" + user.username + "</a></p>";
                    literal.Text += "<p>" + user.date + "</p><hr />";
                    PlaceHolder1.Controls.Add(literal);
                }
            }
            else
            {
                var query = (from u in db.Users where u.userid.ToString().Equals(Request["userid"].ToString()) select u).SingleOrDefault();
                User user = query;
                if (user == null)
                {
                    Literal literal = new Literal();
                    literal.Text = "User not exists";
                    PlaceHolder1.Controls.Add(literal);
                }
                else
                {
                    Label1.Visible = true;
                    Label2.Visible = true;
                    Label3.Visible = true;
                    Label4.Visible = true;
                    Label5.Visible = true;
                    UserName.Text = user.username;
                    Email.Text = user.email;
                    UserName.Visible = true;
                    Email.Visible = true;
                    if (user.firstname != null)
                    {
                        FirstName.Text = user.firstname;
                        FirstName.Visible = true;
                    }
                    if (user.lastname != null)
                    {
                        LastName.Text = user.lastname;
                        LastName.Visible = true;
                    }
                    Registered.Text = user.date.ToString();
                    Registered.Visible = true;
                }
            }
        }
    }
}