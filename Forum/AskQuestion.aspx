﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AskQuestion.aspx.cs" Inherits="Forum.AskQuestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Ask your question here</h1>
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Subject"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" Width="600px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Question"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Width="600px" 
                    Rows="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Category"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" 
                    DataSourceID="LinqDataSource1" DataTextField="categoryname" 
                    DataValueField="categoryname">
                </asp:DropDownList>
                <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
                    ContextTypeName="Forum.DataClassesDataContext" EntityTypeName="" 
                    OrderBy="categoryname" Select="new (categoryname)" TableName="Categories">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:Button ID="Button6" runat="server" Text="Ask Question" />
                
            </td>
        </tr>
    </table>
</asp:Content>
