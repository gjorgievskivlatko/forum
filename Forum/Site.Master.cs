﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedUser"] != null)
            {
                Label1.Text = "Logged as: ";
                UserLink.Text = ((User)Session["LoggedUser"]).username;
                LogOutLink.Visible = true;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void UserLink_Click(object sender, EventArgs e)
        {
            if (Session["LoggedUser"]!=null)
            {
                Response.Redirect("Users.aspx?userid="+((User)Session["LoggedUser"]).userid); 
            }
        }
    }
}
