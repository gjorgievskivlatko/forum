﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = from c in db.Categories select c;
            foreach (Category category in query)
            {
                Literal literal = new Literal();
                literal.Text = "<h2>" + category.categoryname+ "</h2>";
                var query2 = from q in db.Questions where q.Category_categoryid==category.categoryid select q;
                literal.Text += "<p>There are " + query2.Count()+ " questions in this category</p><hr />";
                PlaceHolder1.Controls.Add(literal);
            }
        }
    }
}