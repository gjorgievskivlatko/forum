﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class Questions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = from q in db.Questions select q;
            foreach (Question question in query)
            {
                Literal literal = new Literal();
                literal.Text = "<p>Subject:&nbsp<a href=Answers.aspx?questionid=" + question.questionid + ">" + question.subject + "</a></p>";
                literal.Text += "<p>Text:&nbsp" + question.text + "</p>";
                literal.Text += "<p>Category:&nbsp" + question.Category.categoryname+ "</p>";
                literal.Text += "<p>Date:&nbsp" + question.date + "</p>";
                literal.Text += "<p>Asked:&nbsp<a href=\"Users.aspx?userid="+question.User_userid+"\">" + question.User.username + "</a></p><hr />";
                PlaceHolder1.Controls.Add(literal);
            }
        }
    }
}