﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Forum.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = from d in db.Users where d.username.Equals(LoginUser.UserName.ToString()) select d;
            User u=null;
            foreach(User user in query)
            {
                u=user;
                break;
            }
            if (u!=null)
            {
                if (u.password.Equals(LoginUser.Password))
                {
                    Session["LoggedUser"] = u;
                    if (Session["Page"] == null)
                    {
                        Response.Redirect("/Questions.aspx");
                    }
                    else
                    {
                        Response.Redirect("/" + Session["Page"].ToString());
                    }
                }
                else
                {
                    //Invalid password
                }
            }
            else
            {
                //Invalid username
            }
        }
    }
}
