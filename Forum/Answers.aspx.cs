﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forum
{
    public partial class Answers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["questionid"] != null)
            {
                DataClassesDataContext db = new DataClassesDataContext();
                var query = (from q in db.Questions where q.questionid.ToString().Equals(Request["questionid"].ToString()) select q).Single();
                Question question = query;
                if (question != null)
                {
                    Literal literal = new Literal();
                    literal.Text = "<h1>Question</h1><br/><h2>" + question.subject + "</h2>";
                    literal.Text += "<p>" + question.text + "</p>";
                    literal.Text += "<p>" + question.date + "</p>";
                    literal.Text += "<p>User:&nbsp<a href=\"Users.aspx?userid="+question.User_userid+"\">" + question.User.username + "</a></p><hr />";
                    PlaceHolder1.Controls.Add(literal);
                    literal = new Literal();
                    literal.Text = "";
                    var query2 = from a in db.Answers where a.Question_questionid.ToString().Equals(Request["questionid"].ToString()) select a;
                    bool b = true;
                    foreach (Answer answer in query2)
                    {
                        literal = new Literal();
                        if (b)
                        {
                            literal.Text = "<h1>Answers</h1>";
                            b = false;
                        }
                        literal.Text += "<p>" + answer.text + "</p>";
                        literal.Text += "<p>" + answer.date + "</p>";
                        literal.Text += "<p>User: <a href=\"Users.aspx?userid="+answer.User_userid+"\">" + answer.User.username + "</a></p><hr/>";
                        PlaceHolder1.Controls.Add(literal);
                    }
                    if (Session["LoggedUser"] != null)
                    {
                        literal = new Literal();
                        literal.Text = "<h1>Your Answer</h1>";
                        PlaceHolder1.Controls.Add(literal);
                        TextBox1.Visible = true;
                        Submit.Visible = true;
                    }
                    else
                    {
                        literal = new Literal();
                        literal.Text = "<p><a href=\"Login.aspx\">Your should login to answer</a></p>";
                        PlaceHolder1.Controls.Add(literal);
                    }
                }
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = (from q in db.Questions where q.questionid.ToString().Equals(Request["questionid"].ToString()) select q).Single();
            Question question = query;
            if (question != null)
            {
                Answer answer = new Answer();
                answer.User_userid = ((User)Session["LoggedUser"]).userid;
                answer.Question = question;
                answer.text = TextBox1.Text;
                answer.votes = 0;
                answer.date = DateTime.Now;
                db.Answers.InsertOnSubmit(answer);
                db.SubmitChanges();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }
    }
}